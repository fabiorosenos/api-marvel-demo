# API Marvel Demo

> Simple demonstrative project of The Marvel Api

## How To

### Commands

Execute command:

```sh
$ mvn clean package spring-boot:run
```

### Docker

Feel free:

```sh
$ mvn clean package
$ cd target
$ docker build -t api-marvel-login .
$ docker run -d -p 8082:8082 --name api-marvel api-marvel
```

