FROM openjdk:11
MAINTAINER Fabio Roseno <fabiorosenos@gmail.com>

EXPOSE 8080

ADD target/api-marvel-demo.jar api-marvel-demo.jar

ENTRYPOINT [ "java", "-jar", "api-marvel-demo.jar"]