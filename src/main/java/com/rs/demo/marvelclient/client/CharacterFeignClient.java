package com.rs.demo.marvelclient.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rs.demo.domain.ResultRequest;

@FeignClient(value = "characterClient", url = "${marvel.url}")
public interface CharacterFeignClient {
	
	@GetMapping()
	ResultRequest getResult(@RequestParam Integer offset);
	
	@GetMapping()
	ResultRequest getCharacter(@RequestParam Integer id);
	
}
