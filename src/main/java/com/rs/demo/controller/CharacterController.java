package com.rs.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.rs.demo.domain.Character;
import com.rs.demo.service.CharacterService;

@RestController
public class CharacterController {

	@Autowired
	private CharacterService service;
	
	@GetMapping("/characterIds")
	public List<Integer> getAllIds() {
		return service.listCharacterIds();
	}
	
	@GetMapping("/character/{id}")
	public Character getCharacter(@PathVariable Integer id) {
		return service.getCharacter(id);
	}

}
