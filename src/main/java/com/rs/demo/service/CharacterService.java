package com.rs.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.rs.demo.config.CacheNames;
import com.rs.demo.domain.Character;
import com.rs.demo.domain.Data;
import com.rs.demo.domain.ResultRequest;
import com.rs.demo.marvelclient.client.CharacterFeignClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CharacterService {

	@Autowired
	private CharacterFeignClient client;

	@Cacheable(CacheNames.GET_ALL_CHARACTER_IDS)
	public List<Integer> listCharacterIds() {
		final List<Integer> characterIds = new ArrayList<>();

        final AtomicBoolean hasNextPage = new AtomicBoolean(false);
        final AtomicReference<Integer> offsetAtomic = new AtomicReference<Integer>(0);

        do {

    		final ResultRequest result = client.getResult(offsetAtomic.get());
    		
    		Optional<List<Character>> charLisOptional = Optional.ofNullable(result)
            .map(ResultRequest::getData)
            .map(d -> {
            	offsetAtomic.set(result.getData().getOffset() + result.getData().getCount());
            	hasNextPage.set(result.getData().getTotal() != characterIds.size());
            	return d;
            })
            .map(Data::getResults);
    		
    		charLisOptional.ifPresent(characterList -> {
				for (Character character : result.getData().getResults()) {
					characterIds.add(character.getId());
				}
    		});
        } while(hasNextPage.get());
		
		log.info("Listing characters");
		return characterIds;
	}
	
	public Character getCharacter(Integer id) {
		final ResultRequest result = client.getCharacter(id);

		Optional<List<Character>> characterOptional = Optional.ofNullable(result)
        .map(ResultRequest::getData)
        .map(Data::getResults);
		
		log.info("Get character "+id);
		return characterOptional.filter(s -> !s.isEmpty())
				.map(s -> s.get(0))
				.orElseThrow(IllegalArgumentException::new);
		
	}
	
}
