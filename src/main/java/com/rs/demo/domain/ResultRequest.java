package com.rs.demo.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class ResultRequest {

    @JsonProperty
    private Integer code;
    @JsonProperty
    private String status;
    @JsonProperty
    private String copyright;
    @JsonProperty
    private Data data;

}
