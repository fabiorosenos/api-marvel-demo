package com.rs.demo.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Data {

    @JsonProperty
    private Integer offset;
    @JsonProperty
    private Integer limit;
    @JsonProperty
    private Integer total;
    @JsonProperty
    private Integer count;
    @JsonProperty
    private List<Character> results;
}
